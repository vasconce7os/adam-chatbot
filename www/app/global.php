<?php
session_start();

const C_BASE_URL = 'http://li172-72.members.linode.com/';

//
function escape($str)
{
   return htmlspecialchars($str);
}

//
function myecho($str)
{
   echo escape($str);
}

//
function base_url($uri = '')
{
   return C_BASE_URL . $uri;
}
?>
